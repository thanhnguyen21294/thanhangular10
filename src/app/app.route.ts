import { Routes } from "@angular/router";

export const appRoutes: Routes = [
  {path: '', redirectTo: 'main', pathMatch: 'full'},
  {path: 'main', loadChildren: () => import('src/app/main/main.module').then(m => m.MainModule)}
]
