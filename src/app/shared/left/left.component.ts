import { Component, OnInit } from '@angular/core';
import { SelectService } from 'src/app/core/services/common/select.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-left',
  templateUrl: './left.component.html',
  styleUrls: ['./left.component.css']
})
export class LeftComponent implements OnInit {
  subscription: Subscription;
  constructor(private _selectService: SelectService) { }
  public MenuName: string;

  ngOnInit(): void {
    this.subscription = this._selectService.MenuObservable$.subscribe((res) => {
      this.MenuName = res;
    })
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscription.unsubscribe();
  }

}
