import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { MESSAGE_CONSTANT } from 'src/app/core/constants/message.constant';
import { DataService } from 'src/app/core/services/data.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-modal-books',
  templateUrl: './modal-books.component.html',
  styleUrls: ['./modal-books.component.css']
})
export class ModalBooksComponent implements OnInit, OnDestroy {
  subs = new SubSink();
  public busy: Subscription;
  public entityBooks: any;
  public flagXem: boolean = false;
  public flagSave: boolean = false;
  public flagAdd: boolean = false;
  public flagEdit: boolean = false;
  public max: number = 10;
  public authorList: any[];
  public selectAuthor: any[];

  @Output() SaveSuccess: EventEmitter<any> = new EventEmitter();
  @Output() CloseModal: EventEmitter<any> = new EventEmitter();
  @ViewChild('modalAddEdit') public modalAddEdit: ModalDirective;

  constructor(private _dataService : DataService, private _notification: NotificationService) { }

  ngOnInit() {
    this.getAuthorList();
  }

  ngOnDestroy(): void {
      this.subs.unsubscribe();
  }

  getAuthorList() {
    this.subs.add(
      this._dataService.get("/api/author/getall").subscribe((response: any) => {
        this.authorList = [];
        response.forEach(x=> {
          this.authorList.push({
            id: x.id,
            text: x.fullName
          })
        })
      })
    )
  }

  SelectAuthor() {
    this.entityBooks.AuthorId = this.selectAuthor;
    this.entityBooks.authorName = this.authorList.filter(x=>this.selectAuthor.includes(x.id)).map(y=>y.text);
  }

  OnHidden() {
    this.CloseModal.emit();
  }

  showAddModal() {
    this.showEntityAdd();
    this.modalAddEdit.show();
  }

  showEntityAdd() {
    this.entityBooks = {};
    this.entityBooks.isRead = false;
    this.entityBooks.rate = 0;
    this.selectAuthor = [];
    this.flagSave = false;
    this.flagAdd = true;
    this.flagEdit = false;
  }

  showEditModal(id?: string, flag?: boolean) {
    this.subs.add(
      this._dataService.get("/api/books/getbyid/" + id).subscribe((response: any) => {
        this.entityBooks = response;
        this.selectAuthor = [];
        if(this.entityBooks.authorName.length > 0) {
          this.entityBooks.authorName.forEach(x=> {
            let id = this.authorList.filter(y=>y.text === x).map(y=>y.id).toLocaleString();
            this.selectAuthor.push(parseInt(id));
          })
        }
        this.flagSave = false;
        this.flagAdd = false;
        this.flagEdit = true;
        this.modalAddEdit.show();
      })
    )
  }

  ChangeRating(event) {
    this.entityBooks.rate = event;
  }

  saveChange(form: NgForm, flagThemMoi: boolean) {
    if(form.valid) {
      if(this.entityBooks.id == undefined) {
        this.subs.add(
          this._dataService.post("/api/books/add", this.entityBooks).subscribe((response: any) => {
            if(flagThemMoi) {
              setTimeout(() => {
                this.showEntityAdd();
              }, 50)
            } else {
              this.modalAddEdit.hide();
            }
            this._notification.printSuccessMessage(MESSAGE_CONSTANT.ADD_SUCCESS_MESSAGE);
            this.SaveSuccess.emit(response);
          })
        )
      } else {
        this.subs.add(
          this._dataService.put("/api/books/update/" + this.entityBooks.id, this.entityBooks).subscribe((response: any) => {
            this._notification.printSuccessMessage(MESSAGE_CONSTANT.UPDATE_SUCCESS_MESSAGE)
            this.SaveSuccess.emit(this.entityBooks.id);
            this.modalAddEdit.hide();
          })
        )
      }
    }
  }
}
