import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgBusyModule } from 'ng-busy';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AngularDraggableModule } from 'angular2-draggable';
import { ModalBooksComponent } from './modal-books.component';
import { RatingModule, RatingConfig } from 'ngx-bootstrap/rating';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  imports: [
    CommonModule,
    NgBusyModule,
    FormsModule,
    ModalModule.forRoot(),
    AngularDraggableModule,
    RatingModule,
    TypeaheadModule,
    NgSelectModule
  ],
  providers: [RatingConfig],
  declarations: [ModalBooksComponent],
  exports: [ModalBooksComponent]
})

export class ModalBooksModule {}
