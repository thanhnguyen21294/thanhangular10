import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgBusyModule } from 'ng-busy';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AngularDraggableModule } from 'angular2-draggable';
import { ModalAuthorsComponent } from './modal-authors.component';

@NgModule({
  imports: [
    CommonModule,
    NgBusyModule,
    FormsModule,
    ModalModule.forRoot(),
    AngularDraggableModule
  ],
  declarations:[ModalAuthorsComponent],
  exports:[ModalAuthorsComponent]
})

export class ModalAuthorsModule {}
