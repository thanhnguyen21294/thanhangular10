import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAuthorsComponent } from './modal-authors.component';

describe('ModalAuthorsComponent', () => {
  let component: ModalAuthorsComponent;
  let fixture: ComponentFixture<ModalAuthorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalAuthorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAuthorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
