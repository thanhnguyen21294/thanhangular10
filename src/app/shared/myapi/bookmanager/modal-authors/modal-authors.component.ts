import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { NotificationService } from 'src/app/core/services/notification.service';
import { DataService } from 'src/app/core/services/data.service';
import { MESSAGE_CONSTANT } from 'src/app/core/constants/message.constant';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-modal-authors',
  templateUrl: './modal-authors.component.html',
  styleUrls: ['./modal-authors.component.css']
})
export class ModalAuthorsComponent implements OnInit, OnDestroy {
  subs = new SubSink();
  busy: Subscription;
  public entityAuthors: any;
  public flagXem: boolean = false;
  public flagSave: boolean = false;
  public flagAdd: boolean = false;
  public flagEdit: boolean = false;

  @Output() SaveSuccess: EventEmitter<any> = new EventEmitter();
  @Output() CloseModal: EventEmitter<any> = new EventEmitter();

  @ViewChild('modalAddEdit') public modalAddEdit: ModalDirective;

  constructor(private _dataService : DataService, private _notification: NotificationService) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  OnHidden() {
    this.CloseModal.emit();
  }

  showAddModal() {
    this.showEntityAdd();
    this.modalAddEdit.show();
  }

  showEntityAdd() {
    this.entityAuthors = {};
    this.flagSave = false;
    this.flagAdd = true;
    this.flagEdit = false;
  }

  showEditModal(id?: string, flag?: boolean) {
    this.subs.add(
      this._dataService.get("/api/author/getauthorwithbook/" + id).subscribe((response: any) => {
        this.entityAuthors = response;
        this.flagSave = false;
        this.flagAdd = false;
        this.flagEdit = true;
        this.modalAddEdit.show();
      })
    )
  }

  saveChange(form: NgForm, flagThemMoi: boolean) {
    if(form.valid) {
      if(this.entityAuthors.id == undefined) {
        this.subs.add(
          this._dataService.post("/api/author/add", this.entityAuthors).subscribe((response: any) => {
            if(flagThemMoi) {
              setTimeout(() => {
                this.showEntityAdd();
              }, 50)
            } else {
              this.modalAddEdit.hide();
            }
            this._notification.printSuccessMessage(MESSAGE_CONSTANT.ADD_SUCCESS_MESSAGE);
            this.SaveSuccess.emit(response);
          })
        )
      } else {
        this.subs.add(
          this._dataService.put("/api/author/update/" + this.entityAuthors.id, this.entityAuthors).subscribe((response: any) => {
            this._notification.printSuccessMessage(MESSAGE_CONSTANT.UPDATE_SUCCESS_MESSAGE)
            this.SaveSuccess.emit(response);
            this.modalAddEdit.hide();
          })
        )
      }
    }
  }
}
