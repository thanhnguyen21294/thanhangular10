import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalBooksauthorsComponent } from './modal-booksauthors.component';

describe('ModalBooksauthorsComponent', () => {
  let component: ModalBooksauthorsComponent;
  let fixture: ComponentFixture<ModalBooksauthorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalBooksauthorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalBooksauthorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
