export const URL_CONSTANT = {
  DASHBOARD: "/chucnang/dashboard",
  DITICH: "/chucnang/ditich",
  LOGIN: "/login",
};

export const URL_ROLE = {
  ROLE_CHUYEN_VIEN_QUANTRI: ["hethong", "web"],
  ROLE_CHUYEN_VIEN_NHAP_LIEU_WEB: ["web"],
  ROLE_CHUYEN_VIEN_NHAP_LIEU: ["ditich", "divat","chuyende", "trinhchieu","trungbay", "thongke", "khotulieu","bandoditich","bandodivat"]
};
