export const MESSAGE_CONSTANT = {
  SAVE_SUCCESS_MESSAGE: "Save Success!",
  SAVE_ERROR_MESSAGE: "Save Error!",
  DELETE_SUCCESS_MESSAGE: "Delete Success!",
  DELETE_ERROR_MESSAGE: "Delete Error!",
  DELETE_CONFIRMATION_MESSAGE: "Do you want to delete ?",
  ADD_SUCCESS_MESSAGE: "Add Success!",
  ADD_ERROR_MESSAGE: "Add Error!",
  UPDATE_SUCCESS_MESSAGE: "Update Success!",
  UPDATE_ERROR_MESSAGE: "Update Error!"
}
