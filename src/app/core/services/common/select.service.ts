import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Subject } from "rxjs";

@Injectable()
export class SelectService {
  private notify = new Subject<any>();
  MenuObservable$ = this.notify.asObservable();

  public notifyMenu(data : any) {
    if(data) {
      this.notify.next(data);
    }
  }
}
