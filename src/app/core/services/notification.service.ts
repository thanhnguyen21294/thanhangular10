import { Injectable } from '@angular/core';
import * as alertify from 'alertifyjs'
// declare var alertify: any;
@Injectable()
export class NotificationService {
  private _notifier: any = alertify;
  constructor() {
    alertify.defaults = {
      // dialogs defaults
      autoReset: true,
      basic: false,
      closable: true,
      closableByDimmer: true,
      frameless: false,
      maintainFocus: true, // <== global default not per instance, applies to all dialogs
      maximizable: true,
      modal: true,
      movable: true,
      moveBounded: false,
      overflow: true,
      padding: true,
      pinnable: true,
      pinned: true,
      preventBodyShift: false, // <== global default not per instance, applies to all dialogs
      resizable: true,
      startMaximized: false,
      transition: 'pulse',

      // notifier defaults
      notifier: {
        // auto-dismiss wait time (in seconds)
        delay: 5,
        // default position
        position: 'bottom-right',
        // adds a close button to notifier messages
        closeButton: false
      },

      // language resources
      glossary: {
        // dialogs default title
        title: 'Confirmation',
        // ok button text
        ok: 'Ok',
        // cancel button text
        cancel: 'Cancel'
      },

      // theme settings
      theme: {
        // class name attached to prompt dialog input textbox.
        input: 'ajs-input',
        // class name attached to ok button
        ok: 'ajs-ok',
        // class name attached to cancel button
        cancel: 'ajs-cancel'
      },
      // global hooks
      hooks:{
        // invoked before initializing any dialog
        preinit:function(instance){},
        // invoked after initializing any dialog
        postinit:function(instance){},
    },
    };
  }

  printSuccessMessage(message: string) {

    this._notifier.success(message);
  }


  pushNotification(message: string) {
    this._notifier.set('notifier', 'position', 'top-right');
    this._notifier.success(message);
  }

  printOk(message: string) {
    this._notifier.alert('Thông báo', message, function () { alertify.success('Ok'); });
  }

  printErrorMessage(message: string) {
    this._notifier.error(message);
  }
  prinWarningMessage(message: string) {
    this._notifier.warning(message);
  }

  printConfirmationDialog(message: string, okCallback: () => any ) {
    this._notifier.confirm(message, function (e) {
      if (e) {
        okCallback();
      } else {
      }
    });
  }

}
