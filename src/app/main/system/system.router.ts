import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { GenreComponent } from "./genre/genre.component";
import { FunctionComponent } from "./function/function.component";

const routes: Routes = [
  {path: '', redirectTo: 'function', pathMatch: 'full'},
  {path: 'function', component: FunctionComponent},
  {path: 'genre', component: GenreComponent}
]

@NgModule({
  imports : [RouterModule.forChild(routes)],
  exports : [RouterModule]
})

export class SystemRouterModule {}
