export interface Books {
  id: number,
  title: string,
  description: string,
  isRead: boolean,
  dateRead: string,
  dateAdded: string,
  rate: number,
  genre: string,
  coverUrl: string
}

export interface BookDetail {
  id: number,
  title: string,
  description: string,
  isRead: boolean,
  dateRead: string,
  dateAdded: string,
  rate: number,
  genre: string,
  coverUrl: string,
  authorName: string[]
}
