import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { DataService } from '../../../core/services/data.service';
import { select, Store } from '@ngrx/store';
import { BookState } from './book-state/book.reducer';
import { switchMap, takeUntil } from 'rxjs/operators';
import { GetAllBooks } from './book-state/book.actions';

@Component({
  selector: 'app-book-api',
  templateUrl: './book-api.component.html',
  styleUrls: ['./book-api.component.css']
})
export class BookApiComponent implements OnInit {
  private readonly onDestroy = new Subject<void>();
  allBooks: any[] = [];

  constructor(private service: DataService, private store: Store<BookState>) { }

  ngOnInit(): void {
    this.GetAllBook();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  GetAllBook() {
    this.store.pipe(
      takeUntil(this.onDestroy),
      select(GetAllBooks),
      switchMap(books => {
        return this.service.get("/api/Books/getall")
      })
    ).subscribe(res => {
      this.allBooks = res;
      console.log(this.allBooks)
    })
  }
}
