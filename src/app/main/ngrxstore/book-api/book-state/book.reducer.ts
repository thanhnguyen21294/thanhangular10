import { createReducer, on } from "@ngrx/store";
import { BookDetail, Books } from "../book-interface/book-api.interface"
import { GetAllBooks, GetBookDetail } from "./book.actions"

export interface BookState {
  books: Books,
  bookDetail: BookDetail
}

const initialState: BookState = {
  books: null,
  bookDetail: null
}

export const bookReducer = createReducer(
  initialState,
  on(GetAllBooks, (state) => ({ ...state })),
  on(GetBookDetail, (state, { bookId }) => {
    if(state.books.id === bookId) return state;

    return [state, bookId]
  })
)
