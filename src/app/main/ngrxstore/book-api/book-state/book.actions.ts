import { createAction, props } from "@ngrx/store";
import { Books, BookDetail } from "../book-interface/book-api.interface"

export const GetAllBooks = createAction(
  '[GetAll]Get all books'
)

export const GetBookDetail = createAction(
  '[GetDetail] Get book detail',
  props<{ bookId: number }>()
)

export const CreateBook = createAction(
  '[CreateBook] Create book',
  props<{ book: BookDetail }>()
)

export const UpdateBook = createAction(
  '[UpdateBook] Update book',
  props<{ bookId: number, book: BookDetail }>()
)

export const DeleteBook = createAction(
  '[DeleteBook] Delete Book',
  props<{ bookId: number }>()
)
