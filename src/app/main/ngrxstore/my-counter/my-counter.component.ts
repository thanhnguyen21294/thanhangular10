import { Component } from "@angular/core";
import { Observable } from "rxjs";
import { increment, decrement, reset } from '../counter.action';
import { Store } from '@ngrx/store';
//Store để dispatch các hành động

@Component({
  selector: 'app-my-counter',
  templateUrl: './my-counter.component.html',
})

//Thành phần này sẽ hiển thị các nút cho phép người dùng thay đổi trạng thái đếm
export class MyCounterComponent {
  count$ : Observable<number>;

  constructor(private store: Store<{count: number}>) {
    // TODO: Connect `this.count$` stream to the current store `count` state
    this.count$ = store.select('count');
  }

  increment() {
    // TODO: Dispatch an increment action
    this.store.dispatch(increment());
  }

  decrement() {
    // TODO: Dispatch a decrement action
    this.store.dispatch(decrement());
  }

  reset() {
    // TODO: Dispatch a reset action
    this.store.dispatch(reset());
  }
}
