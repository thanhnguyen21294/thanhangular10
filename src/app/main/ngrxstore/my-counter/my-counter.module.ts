import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MyCounterComponent } from "./my-counter.component";

@NgModule({
  imports :[
    CommonModule
  ],
  declarations: [
    MyCounterComponent
  ],
  exports: [
    MyCounterComponent
  ]
})

export class MyCounterModule {}
