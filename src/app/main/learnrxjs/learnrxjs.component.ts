import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-learnrxjs',
  templateUrl: './learnrxjs.component.html',
  styleUrls: ['./learnrxjs.component.css']
})
export class LearnrxjsComponent implements OnInit {
  count: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  onIncrement() {
    this.count++;
  }

  onDecrement() {
    this.count--;
  }
}
