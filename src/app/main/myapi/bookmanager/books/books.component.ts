import { Component, Input, OnDestroy, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { MESSAGE_CONSTANT } from 'src/app/core/constants/message.constant';
import { DataService } from 'src/app/core/services/data.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { ModalBooksComponent } from 'src/app/shared/myapi/bookmanager/modal-books/modal-books.component';
import { SYSTEM_CONSTANT } from 'src/app/core/constants/system.constant';
import { SubSink } from 'subsink';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit, OnDestroy {
  subs = new SubSink();
  busy: Subscription;
  public listBooks: any[] = [];
  public authorList: any[];
  public filter: string = "";
  public viewModal: boolean = false;
  public flagEdit: boolean = false;
  public flagDelete: boolean = false;
  public maxSize: number = SYSTEM_CONSTANT.pageDisplay;
  public pageIndex: number = SYSTEM_CONSTANT.pageIndex;
  public pageSize: number = SYSTEM_CONSTANT.pageSize;
  public totalRows: number;
  public idAuthor: string;

  @Input() isAuthorChange: any;
  @ViewChild(ModalBooksComponent) public modalAddEdit : ModalBooksComponent;
  constructor(private _dataService: DataService, private _notification: NotificationService) { }

  ngOnInit(): void {
    this.loadBooks();
    this.loadAuthorList();
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Called before any other lifecycle hook. Use it to inject dependencies, but avoid any serious work here.
    //Add '${implements OnChanges}' to the class.
    if(this.isAuthorChange) {
      this.loadBooks();
      this.loadAuthorList();
    }
  }

  loadBooks(emitId?: number, currentPage? : number) {
    if(currentPage) {
      this.pageIndex = currentPage;
    }
    if(this.idAuthor == undefined) {
      this.idAuthor = null;
    }
    this.subs.add(
      this._dataService.get("/api/Books/getlistpaging?page=" + this.pageIndex + "&pageSize=" + this.pageSize + "&filter=" + this.filter +"&idAuthor=" + this.idAuthor).subscribe((response: any) => {
        this.listBooks = response.items;
        this.pageIndex = response.pageIndex;
        this.pageSize = response.pageSize;
        this.totalRows = response.totalRows;
        if(emitId) {
          this.listBooks.filter(x=>x.id == emitId)[0].Checked = true;
          this.flagEdit = false;
          this.flagDelete = false;
        } else {
          this.listBooks[0].Checked = true;
          this.flagEdit = false;
          this.flagDelete = false;
        }
      })
    )
  }

  loadAuthorList() {
    this.subs.add(
      this._dataService.get("/api/author/getall").subscribe((response: any) => {
        this.authorList = [];
        response.forEach(x=> {
          this.authorList.push({
            id: x.id,
            text: x.fullName
          })
        })
      })
    )
  }

  clearFilter() {
    this.filter = "";
    this.loadBooks();
  }

  showAdd() {
    this.viewModal = true;
    setTimeout(() => {
      this.modalAddEdit.showAddModal();
    }, 100)
  }

  showEdit() {
    this.viewModal = true;
    let id = this.listBooks.filter(x=>x.Checked);
    setTimeout(() => {
      this.modalAddEdit.showEditModal(id[0].id);
    })
  }

  deleteBook() {
    let check = this.listBooks.filter(x=>x.Checked).map(x=>x.id);
    this._notification.printConfirmationDialog(MESSAGE_CONSTANT.DELETE_CONFIRMATION_MESSAGE, () => {
      this.confirmationdeleteBook(check);
    })
  }

  confirmationdeleteBook(listId: any[]) {
    this.subs.add(
      this._dataService.delete("/api/Books/delete", "listid", JSON.stringify(listId)).subscribe((res: any) => {
        this._notification.printSuccessMessage(MESSAGE_CONSTANT.DELETE_SUCCESS_MESSAGE);
        this.loadBooks();
      })
    )
  }

  ClickRowBook(item) {
    item.Checked = !item.Checked;
    let checked = this.listBooks.filter(x=>x.Checked);
    if(checked.length == 1) {
      this.flagEdit = false;
      this.flagDelete = false;
    } else if(checked.length > 1){
      this.flagEdit = true;
      this.flagDelete = false;
    } else if(checked.length == 0) {
      this.flagEdit = true;
      this.flagDelete = true;
    }
  }

  CloseModal() {
    this.viewModal = false;
  }

  checkedAll(ev) {
    this.listBooks.forEach(x=>x.Checked = ev.target.checked);
    this.Icheck();
    this.flagEdit = true;
    let checks = this.listBooks.filter(x=>x.Checked);
    if(checks.length > 0) {
      this.flagDelete = false;
    } else {
      this.flagDelete = true;
    }
  }

  Icheck() {
    let checks = this.listBooks.filter(x => x.Checked);
  }

  isCheckedAll() {
    if(this.listBooks) {
      return this.listBooks.every(x=>x.Checked);
    }
  }

  SaveSuccess(event) {
    if(event) {
      this.loadBooks(event);
    } else {
      this.loadBooks();
    }
  }

  pageChanged(event: any): void {
    this.pageIndex = event.page;
    this.loadBooks(null);
  }

  SelectAuthor(event, num? : number) {
    this.idAuthor = event;
    this.loadBooks(null, num);
  }
}
