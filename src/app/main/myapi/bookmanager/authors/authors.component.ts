import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { DataService } from 'src/app/core/services/data.service';
import { ModalAuthorsComponent } from 'src/app/shared/myapi/bookmanager/modal-authors/modal-authors.component';
import { MESSAGE_CONSTANT } from 'src/app/core/constants/message.constant';
import { NotificationService } from 'src/app/core/services/notification.service';
import { SYSTEM_CONSTANT } from 'src/app/core/constants/system.constant';
import { SubSink } from 'subsink';
import { debounceTime } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit, OnDestroy {
  subs = new SubSink();
  busy: Subscription;
  public listAuthors: any[] = [];
  public filter: string = "";
  public viewModal: boolean = false;
  public flagEdit: boolean = false;
  public flagDelete: boolean = false;

  public maxSize: number = SYSTEM_CONSTANT.pageDisplay;
  public pageIndex: number = SYSTEM_CONSTANT.pageIndex;
  public pageSize: number = SYSTEM_CONSTANT.pageSize;
  public totalRows: number;

  @Output() SaveAuthor: EventEmitter<any> = new EventEmitter();
  @ViewChild(ModalAuthorsComponent) public modalAddEdit : ModalAuthorsComponent;
  constructor(private _dataService: DataService, private _notification: NotificationService) { }

  ngOnInit(): void {
    this.loadAuthors();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subs.unsubscribe();
  }

  loadAuthors(editId? : number, pageSearch? : number) {
    if(pageSearch) {
      this.pageIndex = pageSearch;
    }
    this.subs.add(
      this._dataService.get("/api/Author/getlistpaging?page=" + this.pageIndex + "&pageSize=" + this.pageSize + "&filter=" + this.filter).subscribe((response: any) => {
        this.listAuthors = response.items;
        this.pageIndex = response.pageIndex;
        this.pageSize = response.pageSize;
        this.totalRows = response.totalRows;
        if(editId) {
          this.listAuthors.filter(x=>x.Id == editId)[0].Checked = true;
          this.flagEdit = false;
          this.flagDelete = false;
        } else {
          if(this.listAuthors) {
            this.listAuthors[0].Checked = true;
            this.flagEdit = false;
            this.flagDelete = false;
          }
        }
      }, error => {
        console.log(error)
      }, () => {
        console.log('complete get authors data')
      })
    )
  }

  clearFilter() {
    this.filter = "";
    this.loadAuthors(null, 1);
  }

  showAdd() {
    this.viewModal = true;
    setTimeout(() => {
      this.modalAddEdit.showAddModal();
    }, 100)
  }

  showEdit() {
    this.viewModal = true;
    let id = this.listAuthors.filter(x=>x.Checked);
    setTimeout(() => {
      this.modalAddEdit.showEditModal(id[0].id);
    })
  }

  deleteAuthor() {
    let check = this.listAuthors.filter(x=>x.Checked).map(x=>x.id);
    this._notification.printConfirmationDialog(MESSAGE_CONSTANT.DELETE_CONFIRMATION_MESSAGE, () => {
      this.confirmationDeleteAuthor(check);
    })
  }

  confirmationDeleteAuthor(listId: any[]) {
    this.subs.add(
      this._dataService.delete("/api/Author/delete", "listid", JSON.stringify(listId)).subscribe((res: any) => {
        this._notification.printSuccessMessage(MESSAGE_CONSTANT.DELETE_SUCCESS_MESSAGE);
        this.SaveAuthor.emit(true);
        this.loadAuthors();
      })
    )
  }

  ClickRowAuthor(item) {
    item.Checked = !item.Checked;
    let checked = this.listAuthors.filter(x=>x.Checked);
    if(checked.length == 1) {
      this.flagEdit = false;
      this.flagDelete = false;
    } else if(checked.length > 1){
      this.flagEdit = true;
      this.flagDelete = false;
    } else if(checked.length == 0) {
      this.flagEdit = true;
      this.flagDelete = true;
    }
  }

  CloseModal() {
    this.viewModal = false;
  }

  checkedAll(ev) {
    this.listAuthors.forEach(x=>x.Checked = ev.target.checked);
    this.Icheck();
    this.flagEdit = true;
    let checks = this.listAuthors.filter(x=>x.Checked);
    if(checks.length > 0) {
      this.flagDelete = false;
    } else {
      this.flagDelete = true;
    }
  }

  Icheck() {
    let checks = this.listAuthors.filter(x => x.Checked);
  }

  isCheckedAll() {
    if(this.listAuthors) {
      return this.listAuthors.every(x=>x.Checked);
    }
  }

  SaveSuccess(event) {
    if(event) {
      this.SaveAuthor.emit(event);
      this.loadAuthors(event.id);
    }
  }

  pageChanged(event: any): void {
    this.pageIndex = event.page;
    this.loadAuthors(null);
  }
}
