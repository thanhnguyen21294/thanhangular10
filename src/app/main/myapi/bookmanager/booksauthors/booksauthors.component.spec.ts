import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BooksauthorsComponent } from './booksauthors.component';

describe('BooksauthorsComponent', () => {
  let component: BooksauthorsComponent;
  let fixture: ComponentFixture<BooksauthorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BooksauthorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BooksauthorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
