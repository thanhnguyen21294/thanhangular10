import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { BookmanagerComponent } from "./bookmanager.component";

const routes: Routes = [
  { path: '', component: BookmanagerComponent }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class BookManagerRouterModule {}
