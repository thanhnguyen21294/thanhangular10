import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyApiRouterModule } from './myapi.router';

@NgModule({
  imports: [
    CommonModule,
    MyApiRouterModule
  ],
  declarations: []
})

export class MyApiModule{ }
