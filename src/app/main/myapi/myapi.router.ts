import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

const routes: Routes = [
  { path: '', redirectTo: 'bookmanager', pathMatch: 'full' },
  { path: 'bookmanager', loadChildren: () => import("../myapi/bookmanager/bookmanager.module").then(m => m.BookManagerModule) }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class MyApiRouterModule {}
