import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { LeftComponent } from '../shared/left/left.component';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from '../shared/header/header.component';
import { mainRoutes } from './main.router';
import { SystemComponent } from './system/system.component';
import { SelectService } from '../core/services/common/select.service';
import { DataService } from '../core/services/data.service';
import { NotificationService } from '../core/services/notification.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(mainRoutes),
  ],
  declarations: [
    MainComponent,
    LeftComponent,
    HeaderComponent,
    SystemComponent,
  ],
  providers: [SelectService, DataService, NotificationService]
})
export class MainModule { }
